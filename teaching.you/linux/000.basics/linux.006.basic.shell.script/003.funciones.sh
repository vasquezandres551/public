
# funcion : tiene el objetivo de agrupar una funcionalidad, las funciones pueden o no pueden recibir parametros, y pueden o no pueden retornar valores

# eje:
# suma : tiene el objetivo de recibir dos numeros y sumarlos


# buenas practicas: 

# en programacion es una muy buena practica crear funciones reutilizables

# funcion : 1 + 2
# fucion parametro1, parametro2 : parametro1 + parametro2


# forma 1

# no recibe parametros, no retorna
hello(){
  echo "1 1"
  echo "hola hola"
}

# no recibe parametros, no retorna
function hello2(){
  echo "2 2"
  echo "hola2 hola2"
}

# si recibe parametros, no retorna
function info(){
  echo "nombre de funcion    --> $0" # retoma el nombre pero del programa
  echo "parametro de funcion --> $1"
  echo "parametros           --> $@"
  echo "cantidad parametros  --> $#"
}

hello
hello2
info linux curso

# consideraciones especiales

# las funciones en bash se compartan un poco diferente a las funciones de los demas lenguajes

#1. las funciones no retornan valores, si no estados
#2. si se requiere retornar un valor, el tomara todos los echos dentro de la funcion como valor a retornar(recibir afuera)

#3. las funciones se declaran, pero no se ejecutan, a no ser que usted llame la funcion


# fucion parametro1, parametro2 : parametro1 + parametro2
function operacion_mala(){
  valor1=$1
  valor2=$2

  let suma=$valor1+valor2
  echo ""
  echo "---analizar desde acá --"
  echo ""
  echo "la suma es: "
  echo $suma
}

resultado=$(operacion_mala 1 3)
echo "que esta retornando funcion mala ---->"
echo "el resultado de la suma es : $resultado"
echo ""

function operacion_buena(){
  valor1=$1
  valor2=$2
  let suma=$valor1+valor2
  echo $suma
}

resultado=$(operacion_buena 1 3)
echo "que esta retornando funcion buena ---->"
echo "el resultado de la suma es : $resultado"
echo $resultado
echo ""
echo ""
echo ""
echo ""
echo ""

function operacion_buena_estado(){
  valor1=$1
  valor2=$2
  let suma=$valor1+valor2
  echo $suma
  # si no le coloco la palabra return el retorna como estado 0(OK)
  return 1 # devuelve estado + no valores
}

resultado=$(operacion_buena_estado 3 3)
echo "el estado es --> $?"
echo "el valor es  --> $resultado"