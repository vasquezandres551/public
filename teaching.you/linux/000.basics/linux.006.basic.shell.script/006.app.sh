# source permite importar un archivo de utilidades. Funciona con rutas relativas o absolutas

# cargo en memoria  las funciones
source 004.utilitarios.sh         # cargando utilitario
source 005.parametros.properties  # cargando variables

numero1=$1
numero2=$2

imprimir_encabezado_consola

echo ""
echo "la suma es : $(suma $numero1 $numero2)"

echo "estamos en el año cargado desde config : $year_process"