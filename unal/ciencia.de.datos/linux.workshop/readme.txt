# install components on ubuntu | docker container
sudo apt-get update
sudo apt-get install csvkit -y

# install components on mac
# if you don't have installed brew, follow the next guide : https://brew.sh/index_es
brew install csvkit

#validate if csvkit has been installed :
csvsql --version

