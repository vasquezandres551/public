[![N|Solid](https://unal.edu.co/typo3conf/ext/unal_skin_default/Resources/Public/images/escudoUnal_black.png)](https://unal.edu.co/)
# **BIG DATA ANALYTICS**
****
This repo was designed to help different students with workshops and different bits of knowledge about big data and others, as support of the course big data analytics dictated by https://github.com/jdvelasq and Fernan Villa


## **Prerequisites**
****
- Have python installed
- have Linux set up(to mac users: enabled to work, windows users: install docker or some hypervisor)
