
# Haga un join de la tabla anterior con el siguiente archivo:

# 1,A
# 2,B
# 3,C
# 24,X
# 25,Y
# 26,Z

import sys


table_persons = []
table_join = {}
for row in sys.stdin:
    if len(row.split(",")) > 2:
        table_persons.append(row)
    else:
        table_join[row.split(",")[0]] = row.split(",")[1]
else:
    for key, value in table_join.items():
        for row in table_persons:
            if row.split(",")[-1].replace("\n", "") == key:
                sys.stdout.write("{},{}".format(row.replace("\n", ""), value))


