
# Ordene el archivo por fecha.

# reference https://docs.python.org/3/howto/sorting.html
import sys
from datetime import datetime

elements = []

def take_element(element):
    return element.split(",")[3]

for row in sys.stdin:
    if int(row.split(",")[0]) <6:
        sys.stdout.write(row)