import sys

# Genere un archivo que contenga únicamente las columnas nombre, apellido, fecha y color.
for row in sys.stdin:
    data = ",".join(row.split(",")[1:5])
    sys.stdout.write("{}\n".format(data))