![geopandas](https://geopandas.org/en/stable/_images/geopandas_logo_green.png)

**GEOPANDAS**

GeoPandas is an open source project to make working with geospatial data in python easier. GeoPandas extends the datatypes used by pandas to allow spatial operations on geometric types. Geometric operations are performed by shapely. Geopandas further depends on fiona for file access and matplotlib for plotting.

The goal of GeoPandas is to make working with geospatial data in python easier. It combines the capabilities of pandas and shapely, providing geospatial operations in pandas and a high-level interface to multiple geometries to shapely. GeoPandas enables you to easily do operations in python that would otherwise require a spatial database such as PostGIS.

**REQUIREENTS**

![google.colab](https://repository-images.githubusercontent.com/304608186/0b06d300-5dc1-11eb-9a33-97a745c89ceb)

This notebook was designed to run on google Colab, for this reason don't need to install anything. to ingress to Google Colab click on [here](https://colab.research.google.com/)

**LICENCE** 
**MIT**
