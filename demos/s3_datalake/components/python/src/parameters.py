import argparse
import os
import sys

def get_args():
    
    # create the top-level parser
    parser = argparse.ArgumentParser(description='[DEMO] Ingestion Process ')
    
    # create the parser for the "engine"
    parser.add_argument('--engine', dest="engine",type=str, choices=["sqlite3", "mongo"], required=True, help="source engine")
       
    # create the parser get the table
    parser.add_argument('--table', dest="table",type=str, required=True, help="table to do the ingestion")
    
    # create the parser get the table
    parser.add_argument('--database', dest="database",type=str, required=False, help="database table")
    
    # if no option set , show help 
    if len(sys.argv) <= 1:
        parser.print_help()
        sys.exit()
    return parser
