
# define process path
ROOT_PARQUET="../data/files/parquet"
ROOT_JSON="../data/files/json"
ROOT_CSV="../data/files/csv"

echo "               .________._____.___ ._______ .______  "
echo "               |    ___/:         |: __   / :_ _   \ "
echo "               |___    \|   \  /  ||  |>  \ |   |   |"
echo "               |       /|   |\/   ||  |>   \| . |   |"
echo "               |__:___/ |___| |   ||_______/|. ____/ "
echo "                  :           |___|          :/      "
echo "                                             :       "
echo "  ______________________________________________________________________ "
echo " |                         DEMO DATA STORAGE                            |"
echo " |----------------------------------------------------------------------|"
echo " |Components :                                                          |"
echo " |* Bash          * Apache Spark    * DataBricks   * sqlite             |"      
echo " |* Python        * AWS             * Mongo                             |" 
echo " |----------------------------------------------------------------------|"
echo " |FOCUS   :                                                             |" 
echo " |create a simple data storage integrating diferents components         |"
echo " |______________________________________________________________________|"
sleep 10
echo ""

echo "cleaning local and aws data ---------------------> [OK]"
# clear data locally
rm -rf $ROOT_PARQUET/* 2> /dev/null
rm -rf $ROOT_JSON/* 2> /dev/null
rm -rf $ROOT_CSV/* 2> /dev/null

# clear data aws
aws s3 rm --recursive s3://unal-demo-raw/ 2> /dev/null
aws s3 rm --recursive s3://unal-demo-intermediate/ 2> /dev/null
aws s3 rm --recursive s3://unal-demo-master/ 2> /dev/null
echo ""

echo "country has been exported -----------------------> [OK] "
python python/app.py --table country --engine sqlite3     
sleep 2

echo "course_type has been exported -------------------> [OK] "
python python/app.py --table course_type --engine sqlite3     
sleep 2

echo "document_type has been exported -----------------> [OK] "
python python/app.py --table document_type --engine sqlite3     
sleep 2

echo "holidays has been exported ----------------------> [OK] "
python python/app.py --table holidays --engine sqlite3     
sleep 2

echo "users has been exported -------------------------> [OK] "
python python/app.py --table users --engine sqlite3     
sleep 2

echo "students_course has been exported ---------------> [OK] "
python python/app.py --table students_course --engine sqlite3     
sleep 2

echo "sportseller has been exported -------------------> [OK] "
python python/app.py --table sportseller --engine mongo --database demo      
sleep 2

echo "sportshop has been exported ---------------------> [OK] "
python python/app.py --table sportshop --engine mongo --database demo         
sleep 2

echo ""
echo "PREPARING AWS PROCCESS ......... "
echo ""
sleep 5

# agregar comandos aws

###### --------

echo "country has been loaded to aws -----------------------> [OK] "
aws s3 cp "$ROOT_PARQUET/country.parquet" s3://unal-demo-raw/sqlite3/default/country/
sleep 2

echo "course_type has been loaded to aws -------------------> [OK] "
aws s3 cp "$ROOT_PARQUET/course_type.parquet" s3://unal-demo-raw/sqlite3/default/course_type/    
sleep 2

echo "document_type has been loaded to aws -----------------> [OK] "
aws s3 cp "$ROOT_PARQUET/document_type.parquet" s3://unal-demo-raw/sqlite3/default/document_type/    
sleep 2

echo "holidays has been loaded to aws ----------------------> [OK] "
aws s3 cp "$ROOT_PARQUET/holidays.parquet" s3://unal-demo-raw/sqlite3/default/holidays/    
sleep 2

echo "users has been loaded to aws -------------------------> [OK] "
aws s3 cp "$ROOT_PARQUET/users.parquet" s3://unal-demo-raw/sqlite3/default/users/   
sleep 2

echo "students_course has been loaded to aws ---------------> [OK] "
aws s3 cp "$ROOT_PARQUET/students_course.parquet" s3://unal-demo-raw/sqlite3/default/students_course/   
sleep 2

echo "sportseller has been loaded to aws -------------------> [OK] "
aws s3 cp "$ROOT_JSON/sportseller.json" s3://unal-demo-raw/mongo/demo/sportseller/     
sleep 2

echo "sportshop has been loaded to aws ---------------------> [OK] "
aws s3 cp "$ROOT_JSON/sportshop.json" s3://unal-demo-raw/mongo/demo/sportshop/        
sleep 2

echo ""
echo "ANALIZING FILES "
sleep 5

echo   "  CSV     FILES"    && ls -lash $ROOT_CSV | awk '{print $10 " " $6}' | column -t -s ' '
echo ""
echo   "  PARQUET FILES"    && ls -lash $ROOT_PARQUET | awk '{print $10 " " $6}' | column -t -s ' '
echo ""
echo   "  JSON    FILES"    && ls -lash $ROOT_JSON | awk '{print $10 " " $6}' | column -t -s ' '
echo ""

echo "PROCESS FINISHED"
