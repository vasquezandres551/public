CREATE DATABASE IF NOT EXISTS analityc;
CREATE TABLE IF NOT EXISTS analityc.registered_users 
WITH (format='parquet', external_location='s3://unal-demo-master/analityc/registered_users') AS
SELECT     usr.id_user    user_id,
           dt.description document_type,
           usr.first_name,
           usr.last_name,
           lower(usr.email) email,
           ct.description,
           sc.enrollment_date,
           ct.num_students,
           ct.max_students,
           usr.active
FROM       users usr
INNER JOIN document_type dt
ON         usr.id_document = dt.id_document
INNER JOIN students_course sc
ON         sc.id_user = usr.id_user
INNER JOIN course_type ct
ON         sc.id_course = ct.id_course;