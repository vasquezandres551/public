
/* Drop Tables */

DROP TABLE IF EXISTS USERS;

/* Create Tables with Primary and Foreign Keys, Check and Unique Constraints */

-- Contiene la informaci�n de los usuarios registrados en el sistema
CREATE TABLE USERS
(
	ID_USER INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, -- Identificador unico secuencial de la tabla
	ID_DOCUMENT INTEGER NOT NULL,
	FIRST_NAME TEXT NOT NULL,
	LAST_NAME TEXT NOT NULL,
	EMAIL TEXT,
	ACTIVE TEXT NOT NULL DEFAULT S,
	CONSTRAINT FK_USERS_DOCUMENT_TYPE FOREIGN KEY (ID_DOCUMENT) REFERENCES DOCUMENT_TYPE (ID_DOCUMENT) ON DELETE No Action ON UPDATE No Action
)
;

/* Create Indexes and Triggers */

CREATE INDEX IDX_USERS_DOCUMENT_TYPE
 ON USERS (ID_DOCUMENT ASC)
;


INSERT INTO USERS VALUES(1,5,'Kelsey','Bush','consectetuer.euismod.est@tinciduntnibhPhasellus.com','N');
INSERT INTO USERS VALUES(2,2,'Kitra','Garcia','aliquet.lobortis@Duis.co.uk','S');
INSERT INTO USERS VALUES(3,1,'Wing','Luna','at.egestas@Aliquamtinciduntnunc.ca','N');
INSERT INTO USERS VALUES(4,3,'Farrah','Long','ut@eratEtiamvestibulum.com','N');
INSERT INTO USERS VALUES(5,3,'Travis','Petersen','quis@augueutlacus.ca','N');
INSERT INTO USERS VALUES(6,3,'Kyla','Schwartz','tincidunt@felis.com','S');
INSERT INTO USERS VALUES(7,3,'Yuri','Browning','aliquet.Proin@euodiotristique.net','S');
INSERT INTO USERS VALUES(8,5,'Taylor','Griffith','dolor.Quisque@nonummyutmolestie.com','S');
INSERT INTO USERS VALUES(9,3,'Callum','Valentine','Etiam.vestibulum@iaculis.net','N');
INSERT INTO USERS VALUES(10,2,'Ocean','Barry','et@commodotincidunt.org','S');
INSERT INTO USERS VALUES(11,2,'Tucker','Gay','dis.parturient.montes@tinciduntdui.com','S');
INSERT INTO USERS VALUES(12,5,'Cyrus','Kane','parturient.montes@ullamcorperDuis.ca','S');
INSERT INTO USERS VALUES(13,1,'Lydia','Mercado','venenatis.lacus.Etiam@ut.ca','S');
INSERT INTO USERS VALUES(14,5,'Mechelle','Logan','Etiam@mifelis.co.uk','N');
INSERT INTO USERS VALUES(15,2,'Gretchen','Ryan','Nullam.ut@Quisquevarius.com','S');
INSERT INTO USERS VALUES(16,3,'Oren','Perry','aliquet.magna.a@placeratorci.com','S');
INSERT INTO USERS VALUES(17,5,'Keegan','Callahan','Phasellus.libero.mauris@non.co.uk','N');
INSERT INTO USERS VALUES(18,0,'Michael','Aguilar','Aliquam@egetipsumDonec.edu','S');
INSERT INTO USERS VALUES(19,0,'Rachel','Humphrey','enim@vulputatemaurissagittis.edu','N');
INSERT INTO USERS VALUES(20,2,'Oleg','Valencia','Aliquam.nisl.Nulla@necanteMaecenas.net','S');
INSERT INTO USERS VALUES(21,1,'Anika','Head','est.congue.a@Suspendissecommodo.net','N');
INSERT INTO USERS VALUES(22,1,'Medge','Rowland','dolor@rutrumeuultrices.org','N');
INSERT INTO USERS VALUES(23,1,'Nayda','Valenzuela','semper.cursus@Integersem.ca','S');
INSERT INTO USERS VALUES(24,1,'Kiara','Salazar','tincidunt.nunc.ac@magnaa.net','S');
INSERT INTO USERS VALUES(25,1,'Carol','Cotton','nascetur.ridiculus@Aliquamultrices.org','N');
INSERT INTO USERS VALUES(26,4,'Felicia','Sweet','sed.turpis.nec@ipsum.com','N');
INSERT INTO USERS VALUES(27,4,'Julie','Allison','Ut.sagittis@lectuspede.net','S');
INSERT INTO USERS VALUES(28,1,'Griffin','Mcgee','libero.at@Duisacarcu.edu','S');
INSERT INTO USERS VALUES(29,4,'Barrett','Robertson','eget.ipsum@nonquam.org','N');
INSERT INTO USERS VALUES(30,5,'Mary','Stein','lectus.justo.eu@nislsemconsequat.co.uk','S');
INSERT INTO USERS VALUES(31,2,'Rana','Ashley','eu@iaculislacus.edu','N');
INSERT INTO USERS VALUES(32,2,'Maisie','Callahan','montes.nascetur.ridiculus@orci.ca','S');
INSERT INTO USERS VALUES(33,5,'Myles','Poole','Suspendisse.sagittis.Nullam@Pellentesquetincidunttempus.org','N');
INSERT INTO USERS VALUES(34,1,'Garrett','Juarez','mollis@Crassedleo.ca','N');
INSERT INTO USERS VALUES(35,1,'Linda','Workman','quam@eulacus.edu','N');
INSERT INTO USERS VALUES(36,0,'Nolan','Mcknight','dui.augue.eu@Maurisnulla.edu','N');
INSERT INTO USERS VALUES(37,5,'Melodie','Irwin','diam.Duis.mi@sapienCras.net','N');
INSERT INTO USERS VALUES(38,4,'Travis','Morin','elit@fermentumfermentum.com','S');
INSERT INTO USERS VALUES(39,3,'Guinevere','Contreras','Proin@magnased.ca','S');
INSERT INTO USERS VALUES(40,1,'Marsden','Kent','adipiscing.elit@Nunc.net','N');
INSERT INTO USERS VALUES(41,2,'Emery','Riggs','dictum@vulputatevelit.com','S');
INSERT INTO USERS VALUES(42,5,'Kane','Cotton','elit.dictum.eu@estMauriseu.org','S');
INSERT INTO USERS VALUES(43,5,'Zorita','Abbott','luctus@apurus.net','S');
INSERT INTO USERS VALUES(44,4,'Karyn','Nelson','dui.in.sodales@congue.org','N');
INSERT INTO USERS VALUES(45,0,'Roth','Guy','eleifend.Cras@dictumPhasellus.net','S');
INSERT INTO USERS VALUES(46,0,'Wyoming','Francis','sed.consequat@ametconsectetueradipiscing.co.uk','S');
INSERT INTO USERS VALUES(47,0,'Fatima','Green','accumsan.convallis@sedsapien.org','S');
INSERT INTO USERS VALUES(48,3,'Troy','Young','nec.tempus.mauris@auctornon.ca','N');
INSERT INTO USERS VALUES(49,5,'Laith','Osborn','dui.Fusce@Maecenas.org','N');
INSERT INTO USERS VALUES(50,4,'Summer','Brennan','et.rutrum@commodo.edu','N');
INSERT INTO USERS VALUES(51,2,'Isabelle','Nolan','est.ac.mattis@atfringilla.edu','N');
INSERT INTO USERS VALUES(52,1,'Cara','Tran','rutrum.eu.ultrices@neque.co.uk','N');
INSERT INTO USERS VALUES(53,4,'Theodore','Mccray','placerat.Cras.dictum@adipiscing.com','S');
INSERT INTO USERS VALUES(54,0,'Martena','Knox','aliquet.sem@Suspendissesed.ca','S');
INSERT INTO USERS VALUES(55,2,'Aladdin','Byers','Maecenas.iaculis@augue.net','S');
INSERT INTO USERS VALUES(56,2,'Keely','Miranda','vel.mauris.Integer@Sedidrisus.co.uk','N');
INSERT INTO USERS VALUES(57,3,'Marshall','Washington','in.tempus@lacusEtiam.ca','N');
INSERT INTO USERS VALUES(58,0,'Lael','Mccoy','Aliquam@Quisquelibero.org','S');
INSERT INTO USERS VALUES(59,2,'Hamish','Lynch','ut@tellusloremeu.co.uk','S');
INSERT INTO USERS VALUES(60,2,'Hunter','Morris','a@Donecporttitor.edu','N');
INSERT INTO USERS VALUES(61,2,'Micah','Townsend','nonummy@sitamet.org','S');
INSERT INTO USERS VALUES(62,1,'Salvador','Haney','id.nunc.interdum@semper.net','N');
INSERT INTO USERS VALUES(63,3,'Macy','Franco','malesuada@fermentumvelmauris.ca','S');
INSERT INTO USERS VALUES(64,4,'Garrett','Delaney','at.nisi.Cum@habitantmorbitristique.org','S');
INSERT INTO USERS VALUES(65,4,'Jonah','Mcmahon','diam@aliquetmagna.com','N');
INSERT INTO USERS VALUES(66,2,'Gavin','Holman','amet@etnuncQuisque.net','N');
INSERT INTO USERS VALUES(67,4,'Blake','Leach','id.sapien.Cras@odiotristique.co.uk','S');
INSERT INTO USERS VALUES(68,5,'Phillip','Duran','Nunc@Vestibulum.org','N');
INSERT INTO USERS VALUES(69,4,'Vivien','Norris','Praesent@tellussemmollis.co.uk','S');
INSERT INTO USERS VALUES(70,2,'Price','Baldwin','velit@natoque.org','N');
INSERT INTO USERS VALUES(71,3,'Rinah','Cline','nisi.Aenean@est.net','S');
INSERT INTO USERS VALUES(72,4,'Phillip','Mcintosh','vel.lectus@cubilia.org','N');
INSERT INTO USERS VALUES(73,1,'Bell','Castro','euismod.et@Duissit.net','S');
INSERT INTO USERS VALUES(74,3,'Aurora','Lowe','eros.turpis.non@auctor.com','N');
INSERT INTO USERS VALUES(75,4,'Heather','Eaton','mauris.ipsum.porta@arcuac.org','S');
INSERT INTO USERS VALUES(76,0,'Nayda','Sharp','urna.Ut@hendreritaarcu.ca','S');
INSERT INTO USERS VALUES(77,0,'Hadassah','Jarvis','est@nequepellentesque.edu','S');
INSERT INTO USERS VALUES(78,0,'Hillary','Weaver','ipsum@SuspendisseeleifendCras.org','S');
INSERT INTO USERS VALUES(79,1,'Frances','Whitehead','odio.auctor.vitae@Cum.ca','N');
INSERT INTO USERS VALUES(80,4,'Anthony','Bates','odio.tristique@milaciniamattis.co.uk','S');
INSERT INTO USERS VALUES(81,1,'Charity','Berry','non.lobortis.quis@enimcommodo.net','N');
INSERT INTO USERS VALUES(82,3,'Xavier','Woodard','egestas.ligula@Pellentesqueultriciesdignissim.net','N');
INSERT INTO USERS VALUES(83,1,'Fulton','Winters','mauris.id.sapien@sit.org','S');
INSERT INTO USERS VALUES(84,5,'Gwendolyn','Carey','penatibus.et.magnis@primis.com','N');
INSERT INTO USERS VALUES(85,5,'Aubrey','Summers','nec.ante.Maecenas@diamnuncullamcorper.co.uk','N');
INSERT INTO USERS VALUES(86,1,'Tate','Acosta','dictum@nulla.org','S');
INSERT INTO USERS VALUES(87,5,'Jordan','Bailey','Nulla.aliquet.Proin@Cras.edu','N');
INSERT INTO USERS VALUES(88,2,'Alana','Key','mauris@sollicitudinadipiscing.co.uk','N');
INSERT INTO USERS VALUES(89,3,'Elliott','Garza','Sed@orciadipiscingnon.net','N');
INSERT INTO USERS VALUES(90,2,'Palmer','Johns','feugiat.tellus@leo.net','N');
INSERT INTO USERS VALUES(91,0,'Chanda','Green','elit.pharetra@ullamcorperviverraMaecenas.ca','N');
INSERT INTO USERS VALUES(92,1,'Basil','Spence','Phasellus@Maurisblanditenim.com','N');
INSERT INTO USERS VALUES(93,5,'Avye','Delaney','eu@aptenttacitisociosqu.co.uk','S');
INSERT INTO USERS VALUES(94,4,'Cade','Castaneda','non.magna@dictum.edu','N');
INSERT INTO USERS VALUES(95,4,'Rajah','Hicks','mauris@nibhAliquamornare.ca','S');
INSERT INTO USERS VALUES(96,0,'Jaquelyn','Cooley','non.lorem@cursusnonegestas.ca','N');
INSERT INTO USERS VALUES(97,0,'Alyssa','Booker','dictum.magna.Ut@semperduilectus.co.uk','N');
INSERT INTO USERS VALUES(98,0,'Haley','Greer','fermentum.vel@necmollis.org','N');
INSERT INTO USERS VALUES(99,3,'Nathan','Gallagher','nisl@natoque.co.uk','N');
INSERT INTO USERS VALUES(100,0,'Cleo','Banks','nec.euismod@elementumat.com','N');
COMMIT;