// se define la base de datos a usar
use demo;
db.runCommand( { setParameter: 1, logLevel: 2 } )

// se eliminan elementos de la coleccion si existen
db.sportseller.remove({});

// se inserta los elementos de la consulta

// documento # 1
db.sportseller.insert({
    idcliente: 21,
    dpto: 5,
    sexo: 'F',
    comisiones: [10, 15, 20]
});

// documento # 2
db.sportseller.insert({
    idcliente: 23,
    dpto: 5,
    sexo: 'F',
    comisiones: [10, 5]
});

// documento # 3
db.sportseller.insert({
    idcliente: 11,
    dpto: 5,
    sexo: 'M',
    comisiones: [10]
});

// documento # 4
db.sportseller.insert({
    idcliente: 54,
    dpto: 88,
    sexo: 'M',
    comisiones: [5, 5]
});

// documento # 5
db.sportseller.insert({
    idcliente: 763,
    dpto: 88,
    sexo: 'M',
    comisiones: [30]
});
