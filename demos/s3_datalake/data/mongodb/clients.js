// se define la base de datos a usar
use demo;

// se eliminan elementos de la coleccion si existen
db.sportshop.remove({});

// se inserta los elementos de la consulta

// documento # 1
db.sportshop.insert({
    idcliente: 19,
    nombre: 'Angelina',
    preferencias: ['Voleibol', 'Natacion', 'Ciclismo', 'Natacion', 'Natacion']
});

// documento # 2
db.sportshop.insert({
    idcliente: 21,
    nombre: 'Kim',
    preferencias: ['Voleibol', 'Futbol', 'Futbol', 'Futbol']
});

// documento # 3
db.sportshop.insert({
    idcliente: 33,
    nombre: 'Billie',
    preferencias: ['Beisbol', 'Futbol']
});
    
